#include "file.hpp"

File::File(const boost::filesystem::path& file) : _stream(file), _indentation(0) {

}

File::~File() {
    _stream.close();
}

void File::AddIndentation() {
    _indentation++;
}

void File::RemoveIndentation() {
    if(_indentation == 0) throw std::runtime_error("indentation is already 0");

    _indentation--;
}

void File::WriteLine(const std::string& line) {
    for(auto i = 0; i < _indentation; i++) {
        _stream << "    ";
    }

    _stream << line;

    _stream << "\n";
} 

