#pragma once

#include <string>
#include <map>
#include <vector>
#include <sstream>
#include "tokenizer.hpp"

enum Direction
{
  IN = 1,
  OUT = 2,
  IN_OUT = 3,
};

struct Field
{
  std::string name;
  std::string type;
  bool primitive;
  unsigned int length;
  unsigned int arraySize;
};

struct Type{
    std::string name;
    std::vector<Field> fields;
};

struct Packet
{
  std::string name;
  std::string header;
  Direction direction;
  bool sequence;
  bool dynamic;
  std::map<std::string, Type> types;
  std::vector<Field> fields;
};

struct PacketsFile
{
  std::string namespaceValue;
  std::string targetValue;
  std::string coreDirectory;
  std::vector<Packet> packets;
};

class UnexpectedToken : public std::exception
{
public:
  UnexpectedToken(Token token, std::vector<TokenType> expected);
  virtual const char *what() const throw();

private:
  std::stringstream _err;
  std::string _what;
};

class UnexpectedKeyword : public std::exception
{
public:
  UnexpectedKeyword(std::string keyword, std::vector<std::string> expected);
  virtual const char *what() const throw();

private:
  std::stringstream _err;
    std::string _what;
};

class Parser
{
public:
  Parser();
  PacketsFile Parse(const std::string &file);

private:
  void ParseNamespace(int &index, std::vector<Token> &tokens, PacketsFile &output);
  void ParseTarget(int &index, std::vector<Token> &tokens, PacketsFile &output);
  void ParseCoreDirectory(int &index, std::vector<Token> &tokens, PacketsFile &output);
  Packet ParsePacket(int &index, std::vector<Token> &tokens, PacketsFile &output);
  Field ParseField(int &index, std::vector<Token> &tokens, Packet &packet);
};